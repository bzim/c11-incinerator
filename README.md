C11 implementation of incinerator.

Original implementation in Rust:
<https://gitlab.com/bzim/lockfree/blob/master/src/incinerator.rs>.

Documentation written in header file [src/incin.h](src/incin.h).

# Building

Do not use the provided Makefile to build. The provided Makefile is intended to
be used for tests. There are only a single header and a single source code. The
header is intended to be included directly by dependent projects.
Build the source code as you wish. Example:
```zsh
$(cd src && cc -c incin.c)
```
