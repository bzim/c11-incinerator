#include <threads.h>
#include <stdatomic.h>
#include <stddef.h>
#include <stdlib.h>
#include "incin.h"

#define atomic_cas(addr, curr, new)             \
    atomic_compare_exchange_strong_explicit(    \
        addr,                                   \
        curr,                                   \
        new,                                    \
        memory_order_release,                   \
        memory_order_acquire)

struct garbage {
    void *ptr;
    incin_dtor_t dtor;
};

static thread_local struct garbage *garbage = NULL;
static thread_local size_t garbage_len = 0;
static thread_local size_t garbage_cap = 0;

static atomic_ulong volatile pauses;

static void delete_internal(void);

extern inline void incin_force_delete(void);

incin_res_t
incin_add(void *ptr, incin_dtor_t dtor)
{
    if (incin_delete() == INCIN_OK) {
        dtor(ptr);
        return INCIN_OK;
    }

    if (garbage_len == garbage_cap) {
        size_t new_cap = garbage_cap > 0 ? garbage_cap << 1 : 2;
        if (new_cap < garbage_cap) {
            return INCIN_OVERFLOW;
        }
        void *new_alloc = realloc(garbage, new_cap * sizeof(struct garbage));
        if (new_alloc == NULL) {
            return INCIN_NO_MEM;
        }
        garbage = new_alloc;
        garbage_cap = new_cap;
    }

    garbage[garbage_len].ptr = ptr;
    garbage[garbage_len].dtor = dtor;
    garbage_len++;
    return incin_delete();
}

incin_res_t
incin_pause(void)
{
    unsigned long loaded = atomic_load_explicit(&pauses, memory_order_acquire);
    unsigned long new;
    do {
        new = loaded + 1;
        if (new < loaded) return INCIN_OVERFLOW;
    } while (!atomic_cas(&pauses, &loaded, new));
    return INCIN_OK;
}

incin_res_t
incin_resume(void)
{
    unsigned long loaded = atomic_load_explicit(&pauses, memory_order_acquire);
    unsigned long new;
    do {
        new = loaded - 1;
        if (new > loaded) return INCIN_NO_PAUSES;
    } while (!atomic_cas(&pauses, &loaded, new));
    if (new == 0) {
        delete_internal();
        return INCIN_OK;
    }
    return INCIN_PAUSED;
}

incin_res_t
incin_delete(void)
{
    if (atomic_load_explicit(&pauses, memory_order_acquire) == 0) {
        delete_internal();
        return INCIN_OK;
    }
    return INCIN_PAUSED;
}

void
incin_clear_thrd(void)
{
    free(garbage);
    garbage = NULL;
    garbage_len = 0;
    garbage_cap = 0;
}

static void
delete_internal(void)
{
    while (garbage_len > 0) {
        garbage_len--;
        garbage[garbage_len].dtor(garbage[garbage_len].ptr);
    }
}
