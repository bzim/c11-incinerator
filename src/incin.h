#ifndef INCIN_H_
#define INCIN_H_ 1

/*
 * The incinerator.
 *
 * It tries to solve the infamous "ABA problem".
 *
 * It consists of a thread local list of garbage and a counter of pauses.
 * While the counter is != 0, no garbage is deleted.
 */

/*
 * Listage of possible results.
 */
enum incin_res {
    /*
     * No error.
     */
    INCIN_OK,

    /*
     * Some integer overflow (either unsigned or signed) happend.
     */
    INCIN_OVERFLOW,

    /*
     * Allocator failed.
     */
    INCIN_NO_MEM,

    /*
     * Tried to resume an already resumed incinerator.
     */
    INCIN_NO_PAUSES,

    /*
     * Tried to delete the garbage list while it is paused.
     */
    INCIN_PAUSED,
};

/*
 * The type of incinerator results.
 */
typedef enum incin_res incin_res_t;

/*
 * The type of destructors. Just a function accepting a pointer to void.
 */
typedef void (*incin_dtor_t)(void *);

/*
 * Adds a pointer to the garbage list. No action related to destruction, other
 * than calling the destructor with the pointer, is done. No order of
 * destruction can be assumed.
 *
 * # Arguments
 * The first argument (ptr) is the pointer itself.
 * The second argument (dtor) is the destructor.
 *
 * # Return Value
 * - INCIN_OK is returned in case of no errors.
 * - INCIN_PAUSED is returned if the pointer was successfully added to the
 *      garbage list, but could not be destroyed right now because there are
 *      active pauses.
 * - INCIN_OVERFLOW is returned if more space in the garbage list cannot be
 *     made because of integer overflow.
 * - INCIN_NO_MEM is returned if the allocator failed to allocate more space.
 *
 * # Thread Safety
 * This function is thread safe.
 */
extern incin_res_t incin_add(void *ptr, incin_dtor_t dtor);

/*
 * Increments the pause counter. No garbage is deleted while the pause counter
 * is not zero.
 *
 * # Arguments
 * None.
 *
 * # Return Value
 * - INCIN_OK is returned in case of no errors.
 * - INCIN_OVERFLOW is returned if the counter is at its maximum value.
 *
 * # Thread Safety
 * This function is thread safe.
 */
extern incin_res_t incin_pause(void);

/*
 * Decrements the pause counter. Decrementing the counter a different number
 * of times than incrementing may cause undefined behaviour.
 *
 * # Arguments
 * None.
 *
 * # Return Value
 * - INCIN_OK is returned in case of no errors.
 * - INCIN_PAUSED is returned if the new counter value was not 0 and therefore
 *     the garbage list could not be deleted.
 * - INCIN_NO_PAUSES is returned if the counter is currently 0. This is the
 *     only case in which undefined behaviour is not invoked if this function
 *     is miscalled.
 *
 * # Thread Safety
 * This function is thread safe.
 */
extern incin_res_t incin_resume(void);

/*
 * Tries to delete all garbage in the garbage list.
 *
 * # Arguments
 * None.
 *
 * # Return Value
 * - INCIN_OK is returned in case of no errors.
 * - INCIN_PAUSED is returned if the counter value was not 0 and therefore the
 *     garbage list could not be deleted.
 *
 * # Thread Safety
 * This function is thread safe.
 */
extern incin_res_t incin_delete(void);

/*
 * Forces deletion of the garbage queue. This is the only non-lockfree
 * operation, as it spins.
 *
 * # Arguments
 * None.
 *
 * # Return Value
 * None.
 *
 * # Thread Safety
 * This function is thread safe.
 */
inline void incin_force_delete(void);

/*
 * Clears allocated resources by the incinerator. No garbage deletion is
 * performed. If the incinerator was used, this function MUST be called in the
 * end of the thread, otherwise resources will be leaked.
 *
 * # Arguments
 * None.
 *
 * # Return Value
 * None.
 *
 * # Thread Safety
 * This function is thread safe.
 */
extern void incin_clear_thrd(void);

inline void
incin_force_delete(void)
{
    while (incin_delete() != INCIN_OK);
}

#endif
