#include <incin.h>
#include <assert.h>
#include <stddef.h>
#include <threads.h>
#include <stdatomic.h>
#include <stdlib.h>
#include <stdio.h>

#define NTHREAD 20

typedef size_t *_Atomic atomic_ptr;

struct thread_init {
    size_t id;
    atomic_ptr volatile *ptrs;
};

int
thread_main(void *arg)
{
    incin_res_t res;
    struct thread_init *init = arg;
    size_t *alloc = malloc(sizeof(size_t));
    assert(alloc != NULL);
    *alloc = init->id;
    atomic_store(init->ptrs + init->id, alloc);

    assert(incin_pause() == INCIN_OK);
    for (size_t i = 0; i < NTHREAD; i++) {
        size_t *at_index = atomic_load(init->ptrs + i);
        assert(at_index == NULL || *at_index == i);
    }
    res = incin_resume();
    assert(res == INCIN_OK || res == INCIN_PAUSED);

    atomic_store(init->ptrs + init->id, NULL);
    res = incin_add(alloc, free);
    assert(res != INCIN_NO_MEM && res != INCIN_OVERFLOW);

    incin_force_delete();
    incin_clear_thrd();
    return 0;
}

int
main(int argc, char **argv)
{
    thrd_t threads[NTHREAD];
    atomic_ptr volatile ptrs[NTHREAD];
    struct thread_init init[NTHREAD];

    for (size_t i = 0; i < NTHREAD; i++) {
        atomic_init(&ptrs[i], NULL);
        init[i].id = i;
        init[i].ptrs = ptrs;
    }

    for (size_t i = 0; i < NTHREAD; i++) {
        assert(thrd_create(&threads[i], thread_main, &init[i]) == thrd_success);
    }

    for (size_t i = 0; i < NTHREAD; i++) {
        assert(thrd_join(threads[i], NULL) == thrd_success);
    }
    return 0;
}
