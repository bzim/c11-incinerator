#include <incin.h>
#include <stddef.h>
#include <assert.h>

static int touched = 0;

void
touches(void *_arg)
{
    touched = 1;
}

int
main(int argc, char **argv)
{
    assert(incin_add(NULL, touches) == INCIN_OK);
    assert(incin_delete() == INCIN_OK);
    assert(touched);
    incin_clear_thrd();
    return 0;
}
