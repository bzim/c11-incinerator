#include <incin.h>
#include <assert.h>

int
main(int argc, char **argv)
{
    assert(incin_delete() == INCIN_OK);
    assert(incin_pause() == INCIN_OK);
    assert(incin_delete() != INCIN_OK);
    assert(incin_resume() == INCIN_OK);
    assert(incin_delete() == INCIN_OK);
    return 0;
}
