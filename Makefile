CC=clang -fsanitize=address
CFLAGS = -g -O2 -std=c11 -pthread

incin.o : src/incin.c src/incin.h
	$(CC) -o $@ -c $< $(CFLAGS)

tests/% : tests/%.c incin.o
	$(CC) -I src/ -o $@ $^ $(CFLAGS)
